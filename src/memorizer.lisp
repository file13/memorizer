#|

(ql:quickload '(alexandria cl-ppcre ltk))

|#

#|

Memorizer - Michael J. Rossi (dionysius.rossi@gmail.com)
MIT License, 2019

This is a simple program to help memorize large chunks of text.

Here is an example run:

--
CL-USER> (memorizer:cmd-memorize "But his delight is in the law of the LORD; and in his law doth he meditate day and night. Psalm 1:2")
But his delight is in the law of the LORD; and in his law doth he meditate day and night. Psalm 1:2

But his delight is in the law _ the LORD; and in his law doth he meditate day and night. Psalm 1:2

But his delight is _ the law _ the LORD; and in his law doth he meditate day and night. Psalm 1:2

But _ delight is _ the law _ the LORD; and in his law doth he meditate day and night. Psalm 1:2

But _ delight is _ the law _ the LORD; and in his law doth he meditate day and night. _ 1:2

But _ delight is _ the law _ the LORD; and in his _ doth he meditate day and night. _ 1:2

But _ delight is _ the law _ the LORD; and in his _ doth he meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and in his _ doth he meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and in his _ doth _ meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and _ his _ doth _ meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and _ his _ doth _ _ _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and _ his _ doth _ _ _ and night. _ _

But _ _ is _ the law _ the LORD; and _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the LORD; and _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the LORD; _ _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the _ _ _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ is _ the law _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ is _ the _ _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ is _ _ _ _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ _ _ _ _ _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ night. _ _

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

Finished!
NIL
--

The functions work by first breaking a long string into a vector
of words separated by spaces.

We then look for any valid words that are not underscores and create a list of
aref locations.  Next we select a random elt from the arefs.  Finally, we
replace the new random location with an underscore.

The main function is drop-word, which can be called recursively on a string.

|#

(defpackage :memorizer
  (:use :common-lisp :alexandria :cl-ppcre :ltk)
  (:export
   #:drop-word
   #:cmd-memorize
   #:ltk-main
   ))

(in-package :memorizer)

(defun split-string-to-vector (string)
  "Turns a string into a vector of words separated by spaces."
  (coerce (cl-ppcre:split "\\s+" string) 'vector))

(defun valid-arefs (string-vector)
  "Returns a list of valid aref locations in a string-vector. A valid location is anything
which is not an underscore."
  (remove nil
          (loop for i from 0 to (1- (length string-vector))
                collect (let ((string (aref string-vector i)))
                          (if (not (equal string "_"))
                              i)))))
  
(defun random-aref (aref-list)
  "Select a random element."
  (alexandria:random-elt aref-list))

(defun replace-at-aref (string-vector aref)
  "Given a vector of strings and a valid aref, returns a list of the strings with the aref
replaced with an underscore."
  (loop for i from 0 to (1- (length string-vector))
        collect (if (eq i aref)
                    "_"
                    (aref string-vector i))))

(defun drop-random-word (string-vector)
  "Lower level drop function which takes a vector of strings, replaces an element at random,
and returns a list of strings in the same order."
  (let ((aref-list (valid-arefs string-vector)))
    (if aref-list
        (replace-at-aref string-vector (random-aref aref-list))
        aref-list)))

(defun drop-word (string)
  "Main high-level string conversion function which replaces a single random word from a string 
with an underscore.  This can be called recursively until the string is all underscores.
If a string contains all underscores, it will return an empty string."
  (let* ((word-vector (split-string-to-vector string))
         (dropped-list (drop-random-word word-vector)))
    (format nil "~{~A~^ ~}" dropped-list)))

(defun underscore-or-spacep (char)
  "Predicate to determine if a char is a space or underscore."
  (or (char= char #\Space)
      (char= char #\_)))

;;; Command line interface

(defun cmd-shrink-string (string)
  "REPL or command line helper to shrink a string after pressing 'enter' at each step."
  (cond ((every #'underscore-or-spacep string)
         (format t "Finished!~%"))
        (t
         (let ((dropped (drop-word string)))
           (format t "~A~%" dropped)
           (read-line)
           (cmd-shrink-string dropped)))))
         
(defun cmd-memorize (string)
  "Main REPL or command line function to help memorize a string."
  (format t "~A~%" string)
  (read-line)
  (cmd-shrink-string string))

;;; LTK

(defun ltk-prompt-for-string (&optional (string "Enter some text to memorize in this text box and press 'Start Memorizer' to begin."))
  "LTK setup prompt for the user to input the string to memorize."
  (with-ltk ()
    (let* ((f (make-instance 'frame))
           (input (make-instance 'text
                                 :master f
                                 :wrap :word
                                 :wrap :word
                                 :font "Helvetica 15"))
           (b1 (make-instance 'button
                              :master f
                              :text "Start Memorizer"
                              :command (lambda ()
                                         (ltk-memorizer-window (text input)))))
           )
      (wm-title *tk* "Setup Memorizer")
      (setf (text input) string)
      (bind *tk* "<Return>" (lambda (evt)
                              (declare (ignore evt))
                              (ltk-memorizer-window (text input))))
      (pack f :fill :both)
      (pack input :fill :both :expand t)
      (pack b1 :side :top))))

(defmacro ltk-shrink ()
  "A helper macro to shrink a string (drop a word) when the 'Shrink' button is pressed 
(or during other bound keys)."
  `(progn
     (setf string (drop-word string))
     (if (string-equal string "")
         (setf (text words) "All finished.  Great job!")
         (setf (text words) string))))

(defun ltk-memorizer-window (original-string)
  "The main LTK memorizer window.  This lets the user go through the string a word at a time, replacing
each word with an underscore until the entire string is underscores (and should be memorized)."
  (setf *debug-tk* nil)
  (with-ltk ()
    (let* ((string original-string)
           (f (make-instance 'frame))
           (words (make-instance 'text
                                 :master f
                                 :wrap :word
                                 :font "Helvetica 15"
                                 ))
           (b1 (make-instance 'button
                              :master f
                              :text "Shrink"
                              :default :active
                              :command (lambda ()
                                         (ltk-shrink))))
           #|
           (b2 (make-instance 'button
           :master f
           :text "Show Original"
           :command (lambda ()
           (setf (text words) original-string))))
           |#
           (b3 (make-instance 'button
                              :master f
                              :text "Restart"
                              :command (lambda ()
                                         (setf string original-string)
                                         (setf (text words) string))))
           (b4 (make-instance 'button
                              :master f
                              :text "Start New Memorizer"
                              :command (lambda ()
                                         (ltk-prompt-for-string))))
           )
      (wm-title *tk* "Memorizer")
      (setf (text words) string)
      ;; Set the enter/return & space keys to "press" b1
      (bind *tk* "<Return>" (lambda (evt)
                              (declare (ignore evt))
                              (ltk-shrink)))
      (bind *tk* "<space>" (lambda (evt)
                             (declare (ignore evt))
                             (ltk-shrink)))
      (pack f :fill :both)
      (pack words :fill :both :expand t)
      (pack b1 :side :top)
      ;;(pack b2 :side :top)
      (pack b3 :side :top)
      (pack b4 :side :top)
      (configure f :borderwidth 3)
      (configure f :relief :sunken)
      )))

(defun ltk-main ()
  "Main LTK entry point."
  (ltk-prompt-for-string))
