(ql:quickload '(alexandria cl-ppcre ltk trivial-dump-core))
(load "memorizer.lisp")
(trivial-dump-core:save-executable "memorizer.exe" #'memorizer:ltk-main)
