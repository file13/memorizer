(ql:quickload '(alexandria cl-ppcre ltk trivial-dump-core))
(load "memorizer.lisp")
(save-lisp-and-die "memorizer.exe"
                   :toplevel #'memorizer:ltk-main
                   :executable t
                   :application-type :gui
                   )
;;(trivial-dump-core:save-executable "memorizer.exe" #'memorizer:ltk-main)
