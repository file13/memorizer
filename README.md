# Memorizer

This is a simple tool to help in memorizing large chunks of text.
I hacked this up to help my son with Bible verse memorization.

To do so, it succisively removes a random word from a string, replacing it with an underscore, until the entrie
string is eventually all undersocres, and the user can then persumably repeat it without any aid.  The idea is that you keep reading a memorizing the text while the program takes out a random word each time, you will eventually memorize the whole text.

Here's a screenshot of the GUI:
![](https://gitlab.com/file13/memorizer/raw/master/executables/memorizer-screenshot.png "Screenshot")


Included is a [TK Windows GUI executable](https://gitlab.com/file13/memorizer/raw/master/executables/memorizer.zip).  To use this, you must first make sure that TK is installed.

I use [ActiveTcl](https://www.activestate.com/products/tcl/downloads/)

Otherwise, you can use the function cmd-memorize from the REPL (or create something similar for a better command line experience).

# Overview

The program takes a chunk of text and randomly removes a word from it, replacing it with an underscore (_) at each step. The idea is to keep repeating the text as you gradually become less dependant on the text.  Here is what it looks like from the REPL:

```
CL-USER> (memorizer:cmd-memorize "But his delight is in the law of the LORD; and in his law doth he meditate day and night. Psalm 1:2")
But his delight is in the law of the LORD; and in his law doth he meditate day and night. Psalm 1:2

But his delight is in the law _ the LORD; and in his law doth he meditate day and night. Psalm 1:2

But his delight is _ the law _ the LORD; and in his law doth he meditate day and night. Psalm 1:2

But _ delight is _ the law _ the LORD; and in his law doth he meditate day and night. Psalm 1:2

But _ delight is _ the law _ the LORD; and in his law doth he meditate day and night. _ 1:2

But _ delight is _ the law _ the LORD; and in his _ doth he meditate day and night. _ 1:2

But _ delight is _ the law _ the LORD; and in his _ doth he meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and in his _ doth he meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and in his _ doth _ meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and _ his _ doth _ meditate _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and _ his _ doth _ _ _ and night. _ 1:2

But _ _ is _ the law _ the LORD; and _ his _ doth _ _ _ and night. _ _

But _ _ is _ the law _ the LORD; and _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the LORD; and _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the LORD; _ _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the _ _ _ his _ _ _ _ _ and night. _ _

_ _ _ is _ the law _ the _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ is _ the law _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ is _ the _ _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ is _ _ _ _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ _ _ _ _ _ _ _ _ _ his _ _ _ _ _ _ night. _ _

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ night. _ _

_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

Finished!
NIL
```

# Usage

Should be self-explanatory.  Use the Windows executable or else use Common Lisp to load the file `build-exe.lisp`.  This was tested with SBCL and CCL (on Windows 10) and requires quicklisp and Tk.

# License

Copyright (c) 2019 Michael J. Rossi

Licensed under the MIT License.
