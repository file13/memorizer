(defsystem memorizer
  :author "Michael J. Rossi <dionysius.rossi@gmail.com>"
  :maintainer "Michael J. Rossi <dionysius.rossi@gmail.com>"
  :license "MIT"
  :version "0.1"
  :depends-on (:alexandria
               :cl-ppcre
               :ltk
               )
  :components
  ((:module "src"
    :serial t
    :components
    (
     (:file "memorizer")
     )))
  :description "A tool to help with memorizing chunks of text--like Bible verses."
  :long-description #.(uiop:read-file-string
                       (uiop:subpathname *load-pathname* "README.md"))
  )
